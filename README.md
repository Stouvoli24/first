<!DOCTYPE HTML>
<!--
	Aerial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html lang="en">
	<head>
		<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
		<title>Elie Touvoli</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
		<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="/styles.css">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
		<style>
		@import url('https://fonts.googleapis.com/css?family=Lato');


* {
	font-family: 'Courier New', Courier, monospace;
}


body {
	background-image: url('/bg.jpg');
	background-attachment: fixed;
	background-position: center center;
	background-size: 100% 100%;
	
}


.top {
	margin-top: 300px;
}

h1 {
	font-size: 90px;
	color: #d2691e;
	text-align: center;
	
 }



 @keyframes blink {
	from {
	  border: none;
	}
	to {
	  border-color: 2px solid #747474;
	}
 }
 @media screen and (max-width: 670px) {
	h1 {
	  font-size: 40px;
	}
 
	img {
	  width: 75px;
	  height: 75px;
	}
 }
 @media screen and (max-width: 400px) {
	h1 {
	  font-size: 32px;
	}
 }

 /*Styles for Menu */


 .menu {
	text-transform: uppercase;
	color: rgba(255, 255, 255, 0.8);
	display: inline-block;
	cursor: pointer;
	pointer-events: none;
	position: absolute;
	bottom: 20px;
	left: 20px;
 }

 .menu strong {
	 color: orangered;
 }
 .menu:hover {
	pointer-events: all;
 }
 .label {
	display: inline-block;
	cursor: pointer;
	pointer-events: all;
 }
 .spacer {
	display: inline-block;
	width: 80px;
	margin-left: 15px;
	margin-right: 15px;
	vertical-align: middle;
	cursor: pointer;
	position: relative;
 }
 .spacer:before {
	content: "";
	position: absolute;
	border-bottom: 1px solid orangered;
	height: 1px;
	width: 0%;
	transition: width 0.25s ease;
	transition-delay: 0.7s;
 }
 .item {
	position: relative;
	display: inline-block;
	margin-right: 30px;
	top: 10px;
	opacity: 0;
	transition: opacity 0.5s ease, top 0.5s ease;
	transition-delay: 0;
 }
 strong {
	transition: color 0.5s ease;
 }
 .item:hover strong {
	color: #ff0000;
 }
 .menu:hover .spacer:before {
	width: 100%;
	transition-delay: 0s;
 }
 .menu:hover .item {
	opacity: 1;
	top: 0px;
 }
 .item:nth-child(1) {
	transition-delay: 0.45s;
 }
 .item:nth-child(2) {
	transition-delay: 0.4s;
 }
 .item:nth-child(3) {
	transition-delay: 0.35s;
 }
 .item:nth-child(4) {
	transition-delay: 0.3s;
 }
 .item:nth-child(5) {
	transition-delay: 0.25s;
 }
 .item:nth-child(6) {
	transition-delay: 0.2s;
 }
 .item:nth-child(7) {
	transition-delay: 0.15s;
 }
 .item:nth-child(8) {
	transition-delay: 0.1s;
 }
 .item:nth-child(9) {
	transition-delay: 0.05s;
 }
 .item:nth-child(10) {
	transition-delay: 0s;
 }
 .menu:hover .item:nth-child(1) {
	transition-delay: 0.25s;
 }
 .menu:hover .item:nth-child(2) {
	transition-delay: 0.3s;
 }
 .menu:hover .item:nth-child(3) {
	transition-delay: 0.35s;
 }
 .menu:hover .item:nth-child(4) {
	transition-delay: 0.4s;
 }
 .menu:hover .item:nth-child(5) {
	transition-delay: 0.45s;
 }
 .menu:hover .item:nth-child(6) {
	transition-delay: 0.5s;
 }
 .menu:hover .item:nth-child(7) {
	transition-delay: 0.55s;
 }
 .menu:hover .item:nth-child(8) {
	transition-delay: 0.6s;
 }
 .menu:hover .item:nth-child(9) {
	transition-delay: 0.65s;
 }
 .menu:hover .item:nth-child(10) {
	transition-delay: 0.7s;
 }





</style>
		

	</head>
	<body>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- Page Wrapper -->
		<h1 class="top w3-animate-top" >
			<span
			class="txt-rotate"
			data-period="2000"
			data-rotate='[  " Aduna est disponible ", "sur toutes les plateformes" , "enjoy !" ]'></span>
		</h1>

		<div class="menu">
			<div class="label"><strong style="font-size: 16px;">Follow Me</strong></div>
			<div class="spacer"></div>
			<div class="item"><strong>Twitter</strong></div>
			<div class="item"><strong>Instagram</strong></div>
			<div class="item"><strong>Facebook</strong></div>
		</div>

		<section class="w3-content w3-center w3-padding-64 w3-text-deep-orange w3-xxlarge w3-animate-zoom">
			<a href="https://www.facebook.com/profile.php?id=100004891148041" target="_blank"><i class="fa fa-spotify w3-hover-opacity"></i></a>
			<a href="https://music.apple.com/be/artist/g-s-t/1565357126?l=fr" target="_blank"><i class="fa fa-music w3-hover-opacity "></i></a>
			<a href="https://open.spotify.com/artist/4Zxhd3QAvnmxH9bWahALFL?si=aoaAQGLgQQC4bOPK1u9DAw&dl_branch=1" target="_blank"><i class="fa fa-soundcloud w3-hover-opacity"></i></a>
		 <!-- End footer -->
		</section>





	


		<script src="/main.js"></script>

	</body>
</html>